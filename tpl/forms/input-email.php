<div class="input--field email{% if class != "" %} {{ class }}{% endif %}{% if icon != "" %} has-icon{% endif %}">
	<label for="{{ input_id }}"{% if screenreader === true %} class="sr-only"{% else %} class="input--field-label"{% endif %}>{{ label }}</label>
	<input id="{{ input_id }}" name="{{ input_id }}" type="email" placeholder="{{ placeholder }}" value="{{ input_value }}">
    {% if icon != "" %}
    <svg class="icon {{ icon }}"><use xlink:href="#{{ icon }}"></use></svg>
    {% endif %}
	{% if mention %}
    <span class="input--field-help">{{ mention }}</span>
    {% endif %}
</div>

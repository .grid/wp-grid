<div class="input--field-radiogroup">
    {% for radio in radios %}
    {{ formRadio( radio.label, radio.input_id, radio.input_name, radio.checked, radio.class ) }}
    {% endfor %}
</div>

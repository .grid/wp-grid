<?php global $postValues;?>
<div class="input--field <?php echo $args['type'];?> <?php if(isset($args['classes'])) echo $args['classes'];?> <?php if(isset($args['icon']) && !empty($args['icon'])) echo "has-icon";?>">

  <label for="<?php echo $args['key'];?>" class="<?php echo (isset($args['screenreader']) && $args['screenreader'] == "screenreader") ? "sr-only" :  "input--field-label";?>"><?php echo $args['label'];?></label>
	<input id="<?php echo $args['key'];?>" name="<?php echo $args['key'];?>" type="<?php echo $args['type'];?>" placeholder="<?php echo $args['label'];?>" value="<?php if(isset($postValues[$args['key']])) echo $postValues[$args['key']];?>">

  <?php if(isset($args['icon']) && !empty($args['icon'])): ?>
    <svg class="icon <?php echo $args['icon'];?>"><use xlink:href="#<?php echo $args['icon'];?>"></use></svg>
  <?php endif;?>

  <?php if(isset($args['mention']) && !empty($args['mention'])): ?>
    <span class="input--field-help"><?php echo $args['mention'];?></span>
  <?php endif;?>
</div>

<?php global $postValues;?>
<div class="input--field select <?php if(isset($args['classes'])) echo $args['classes'];?> <?php if(isset($args['icon']) && !empty($args['icon'])) echo "has-icon";?>">
	<label for="<?php echo $args['key'];?>"{% if screenreader === true %} class="sr-only"{% else %} class="input--field-label"{% endif %}>{{ label }}</label>
    <div class="select-wrap">
        <select id="<?php echo $args['key'];?>" name="<?php echo $args['key'];?>">
            <?php if(isset($args['placeholder']) && !empty($args['placeholder'])): ?>
              <option value="" selected disabled><?php echo $args['placeholder'];?></option>
            <?php endif;?>
            <?php
            if(isset($args['datas']) && is_array($args['datas']) && !empty($args['datas'])):
              foreach($args['datas'] as $value => $label):
                $checked = (isset($postValues[$args['key']]) && $value == $postValues[$args['key']]);
                ?>
                <option value="<?php echo $value;?>" <?php if($checked) echo 'checked';?>><?php echo $label;?></option>
              <?php
              endforeach;
            endif;
            ?>
        </select>
        <?php if(isset($args['icon']) && !empty($args['icon'])): ?>
          <svg class="icon <?php echo $args['icon'];?>"><use xlink:href="#<?php echo $args['icon'];?>"></use></svg>
        <?php endif;?>
    </div>
    <?php if(isset($args['mention']) && !empty($args['mention'])): ?>
      <span class="input--field-help"><?php echo $args['mention'];?></span>
    <?php endif;?>
</div>

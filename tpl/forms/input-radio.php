<?php
global $postValues;
$name = $args['key'];
?>
<div class="input--field-radiogroup">
  <?php if(isset($args['datas'])): ?>
    <?php
    foreach($args['datas'] as $key => $label):
      $checked = (isset($postValues[$name]) && $key == $postValues[$name]);
      ?>
      <div class="input--field radio <?php if(isset($args['classes'])) echo $args['classes'];?> <?php if($checked) echo 'checked';?>">
        <label for="<?php echo $name.'-'.$key;?>"><input <?php if($checked) echo 'checked';?> id="<?php echo $name.'-'.$key;?>" name="<?php echo $name;?>" value="<?php echo $key;?>" type="radio"> <?php echo $label;?></label>
      </div>
    <?php endforeach;?>
  <?php endif;?>
</div>

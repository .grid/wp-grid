<?php
global $postValues;
$name = $args['key'];
?>
<div class="form--step-choices">
  <?php if(isset($args['datas'])): ?>
    <?php
    foreach($args['datas'] as $key => $label):
      $checked = (isset($postValues[$name]) && ((is_array($postValues[$name]) && in_array($key, $postValues[$name])) || (!is_array($postValues[$name]) && $key == $postValues[$name])));
      ?>
      <div class="checkboxXTD <?php if(isset($args['classes'])) echo $args['classes'];?> <?php if($checked) echo 'checked';?>">
          <div class="checkboxXTD-symbol <?php echo $key;?>">
              <?php include(TEMPLATEPATH . '/images/icons-forms/radio-' . $key.'.svg');?>
          </div>
          <label for="<?php echo $name.'-'.$key;?>" class="checkboxXTD-label"><span><?php echo $label;?></span></label>
          <input <?php if($checked) echo 'checked';?> class="checkboxXTD-input" type="checkbox" value="<?php echo $key;?>" id="<?php echo $name.'-'.$key;?>" name="<?php echo $name;?><?php if(count($args['datas']) > 1) echo '[]';?>">
      </div>
    <?php endforeach;?>
  <?php endif;?>
</div>

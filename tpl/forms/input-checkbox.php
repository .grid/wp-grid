<?php
global $postValues;
$name = $args['key'];
?>
<div class="form--step-choices">
  <?php if(isset($args['datas'])): ?>
    <?php foreach($args['datas'] as $key => $label):
      $checked = (isset($postValues[$name]) && ((is_array($postValues[$name]) && in_array($key, $postValues[$name])) || (!is_array($postValues[$name]) && $key == $postValues[$name])));
      ?>
      <div class="input--field checkbox <?php if(isset($args['classes'])) echo $args['classes'];?> <?php if($checked) echo 'checked';?>">
          <label for="<?php echo $name.'-'.$key;?>"><input <?php if($checked) echo 'checked';?> id="<?php echo $name.'-'.$key;?>" name="<?php echo $name;?><?php if(count($args['datas']) > 1) echo '[]';?>" value="<?php echo $key;?>" type="checkbox"> <?php echo $label;?></label>
      </div>
    <?php endforeach;?>
  <?php endif;?>
</div>

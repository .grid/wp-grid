<?php
global $postValues;
$name = $args['key'];
?>
<div class="form--step-choices">
  <?php if(isset($args['datas'])): ?>
    <?php
    foreach($args['datas'] as $key => $label):
      $checked = (isset($postValues[$name]) && $key == $postValues[$name]);
      ?>
      <div class="radioXTD <?php if(isset($args['classes'])) echo $args['classes'];?> <?php if($checked) echo 'checked';?>">
          <div class="radioXTD-symbol <?php echo $key;?>">
              <?php include(TEMPLATEPATH . '/images/icons-forms/radio-' . $key.'.svg');?>
          </div>
          <label for="<?php echo $name.'-'.$key;?>" class="radioXTD-label"><span><?php echo $label;?></span></label>
          <input <?php if($checked) echo 'checked';?> class="radioXTD-input" type="radio" value="<?php echo $key;?>" id="<?php echo $name.'-'.$key;?>" name="<?php echo $name;?>">
      </div>
    <?php endforeach;?>
  <?php endif;?>
</div>

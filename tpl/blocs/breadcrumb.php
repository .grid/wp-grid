<?php
global $wp_query;
function display_breadcrumb_separator(){
	?>
	<span class="breadcrumb--sep">|</span>
	<?php
}
 ?>
<div class="breadcrumb">
	<div class="breadcrumb--wrap">
			<a href="<?php echo get_home_url();?>"><?php echo __('Accueil', 'grid_lang');?></a>
        <?php

				if(is_home() && $wp_query->is_posts_page){
						display_breadcrumb_separator();
						?>
						<span class="breadcrumb--current"><?php echo get_the_title(ACTUALITES_PAGEID);?></span>
						<?php
				}

        if(is_404()){
						display_breadcrumb_separator();
						?>
						<span class="breadcrumb--current">404</span>
						<?php
        }

        if(is_author()){
						display_breadcrumb_separator();
            $auteur = get_user_by('id',$author);
						?>
						<span class="breadcrumb--current"><?php echo sprintf(__('Auteur : %s', 'grid_lang'), $auteur->display_name);?></span>
						<?php
        }

        if(is_date()){
						display_breadcrumb_separator();
            if ( is_day() ){
							?>
							<span class="breadcrumb--current"><?php echo sprintf(__('Archives - %s', 'grid_lang'), get_the_date('j F Y'));?></span>
							<?php
            }elseif ( is_month() ){
							?>
							<span class="breadcrumb--current"><?php echo sprintf(__('Archives - %s', 'grid_lang'), get_the_date('F Y'));?></span>
							<?php
            }elseif ( is_year() ){
							?>
							<span class="breadcrumb--current"><?php echo sprintf(__('Archives - %s', 'grid_lang'), get_the_date('Y'));?></span>
							<?php
						}

        }


				if(is_tax('cat-produits')){
					display_breadcrumb_separator();
					?>
					<a href="<?php echo get_permalink(NOSPRODUITS_PAGEID);?>"><?php echo get_the_title(NOSPRODUITS_PAGEID);?></a>
					<?php
				}

        // Post types & post
        $cpost_types = get_post_types('','names');
        foreach ($cpost_types as $post_type ) {
						if($post_type == "post" && is_singular('post')){
							display_breadcrumb_separator();
							?>
							<a href="<?php echo get_permalink(ACTUALITES_PAGEID);?>"><?php echo get_the_title(ACTUALITES_PAGEID);?></a>
							<?php
						}

						if(!in_array($post_type,array('revision','attachment','nav_menu_item','page'))){
                $obj = get_post_type_object($post_type);
                // Archive de post type
                if($post_type != 'post' && is_post_type_archive( $post_type )){
										display_breadcrumb_separator();
										?>
										<span class="breadcrumb--current"><?php echo $obj->labels->name;?></span>
										<?php
                }
                // Single
                if(is_singular($post_type)){
										if($post_type != 'post'){
											display_breadcrumb_separator();
											?>
											<a href="<?php echo get_post_type_archive_link($post_type);?>"><?php echo $obj->labels->name;?></a>
											<?php
										}
                    $categories = get_the_category();
                    if(!empty($categories)){
                        $cat_parent = array();
                        foreach($categories as $category)
                            $cat_parent[$category->parent][] = $category;
                        ksort($cat_parent);

                        // On affiche toutes les categories, triées par parent
                        foreach($cat_parent as $categories){
                            foreach($categories as $category){
																display_breadcrumb_separator();
																?>
																<a href="<?php echo get_category_link( $category->cat_ID );?>"><?php echo $category->cat_name;?></a>
																<?php
                            }
                        }
                    }
										display_breadcrumb_separator();
										?>
										<span class="breadcrumb--current"><?php echo get_the_title();?></span>
										<?php
                }
            }
        }

        // Page single
        if(is_page()){
            global $wp_query;
            $ancetres = get_post_ancestors(get_the_ID());
            if(is_array($ancetres)){
                krsort($ancetres);
                foreach($ancetres as $ancetre){
										display_breadcrumb_separator();
										?>
										<a href="<?php echo get_permalink($ancetre);?>"><?php echo get_the_title($ancetre);?></a>
										<?php
                }
            }
						display_breadcrumb_separator();
						?>
						<span class="breadcrumb--current"><?php echo get_the_title();?></span>
						<?php
        }

        // Page de taxonomie
        if(is_category() || is_tag() || is_tax()){
            $term = get_queried_object();
						$copyterm = $term;
						if(isset($copyterm->parent, $copyterm->taxonomy)){
							while($copyterm->parent > 0){
								$copyterm = get_term_by('id', (int)$copyterm->parent, $term->taxonomy);
								display_breadcrumb_separator();
								?>
								<a href="<?php echo get_term_link($copyterm);?>"><?php echo $copyterm->name;?></a>
								<?php
							}
						}
            if(isset($term->taxonomy)){
                $terms_taxonomy = get_taxonomy($term->taxonomy);
                if(isset($terms_taxonomy->labels->singular_name)) {
									display_breadcrumb_separator();
									?>
									<span class="breadcrumb--current"><?php echo single_term_title('', false);?></span>
									<?php
                }
            }
        }

        // Résultat de recherche
        if(is_search()){
					display_breadcrumb_separator();
					?>
					<span class="breadcrumb--current"><?php echo __('Recherche', 'grid_lang');?></span>
					<?php
        }


        ?>
    </div>
</div>

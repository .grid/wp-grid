<?php
global $wp_query, $paged;

$testQuery = $wp_query;
if (isset($args['query'])) {
    $testQuery = $args['query'];
}

// Pagination
$page_num = $paged;
if ($page_num == '') {
    $page_num = '1';
}

$max_num_pages = $testQuery->max_num_pages;
if (isset($stl_the_query) && is_object($stl_the_query)) {
    $max_num_pages = $stl_the_query->max_num_pages;
}

$ancre = "";
if (isset($testQuery->query, $testQuery->query['post_type']) && $testQuery->query['post_type'] == 'press') {
    $ancre = "#press";
}

$pas_pagination = 3;

if ($max_num_pages > 1):
    ?>

    <div class="pagination">
			<div class="pagination--links">
				<ul class="pagination--links-list">

          <?php if ($page_num>1): ?>
            <li class="pagination--prev">
              <a href="<?php echo get_pagenum_link($page_num-1);?><?php echo $ancre;?>">
                <svg class="icon icon-prev"><use xlink:href="#icon-prev"></use></svg>
              </a>
            </li>
          <?php endif;?>


          <?php
          if (is_search()) {
              $s = (isset($_GET['s'])) ? strip_tags($_GET['s']) : '';
          }

          $cat_id = (isset($_GET['cat_id']) && ctype_digit($_GET['cat_id'])) ? '?cat_id='.$_GET['cat_id'] : '';

          for ($i=1;$i<=$max_num_pages;$i++):
              if (($i > ($page_num - $pas_pagination) && $i < ($page_num + $pas_pagination)) || $i == 1 || $i == $max_num_pages):

                  if ($i == $max_num_pages && $page_num < $i - $pas_pagination):
                      ?>
                      <li class="sep"><span>...</span></li>
                  <?php endif;?>

                  <li class="<?php if ($page_num == $i)  echo 'pagination--current';?>"><a href="<?php echo get_pagenum_link($i);?><?php echo $ancre;?>"><?php echo $i;?></a></li>


                  <?php if ($i == 1 && $page_num > $i + $pas_pagination): ?>
                      <li class="sep"><span>...</span></li>
                  <?php endif;?>

                  <?php
              endif;
          endfor;
          ?>

          <?php if ($page_num < $max_num_pages): ?>

            <li class="pagination--next">
              <a href="<?php echo get_pagenum_link($page_num+1);?><?php echo $ancre;?>">
                <svg class="icon icon-next"><use xlink:href="#icon-next"></use></svg>
                </a>
            </li>
          <?php endif;?>
      </ul>
    </div>
<?php
endif;

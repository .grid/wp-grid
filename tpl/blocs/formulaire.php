<?php
global $postValues;
$form = $args['form'];
$fields = $form->getFields();
$reponses = $form->save_form();

if(isset($args['autovalue'])){
  foreach($args['autovalue'] as $key => $value){
    $postValues[$key] = $value;
  }
}
?>

<?php if(!empty($reponses) && isset($reponses['successes']) && !empty($reponses['successes'])): ?>
 <ul class="list-success">
	 <?php foreach($reponses['successes'] as $success): ?>
		 <li><?php echo $success;?></li>
	 <?php endforeach;?>
 </ul>

<?php endif; ?>


<?php
if(!empty($reponses) && isset($reponses['errors']) && !empty($reponses['errors'])): ?>
 <ul class="list-errors">
	 <?php foreach($reponses['errors'] as $error): ?>
		 <li><?php echo $error;?></li>
	 <?php endforeach;?>
 </ul>
<?php endif;?>

<form method="post" action="" enctype="multipart/form-data" class="form--project-register">

    <div class="form--mentions">
        <sup>*</sup><?php echo __('Champ obligatoire', 'grid_lang');?>
    </div>

    <?php
    foreach($fields as $key => $info):
      $args = array_merge($info, array('key' => $key));

      if(isset($args['pretext'])){
        echo $args['pretext'];
      }

      switch($info['type']):
        case 'hidden' :
          echo grid_get_template_part('tpl/forms', 'input-hidden', $args);
        break;

        case 'text' :
        case 'email' :
          echo grid_get_template_part('tpl/forms', 'input-text', $args);
        break;

        case 'checkbox' :
          echo grid_get_template_part('tpl/forms', 'input-checkbox' . ((isset($info['format']) && !empty($info['format'])) ? '-'.$info['format'] : ''), $args);
        break;

        case 'radio' :
          echo grid_get_template_part('tpl/forms', 'input-radio' . ((isset($info['format']) && !empty($info['format'])) ? '-'.$info['format'] : ''), $args);
        break;

        case 'select' :
          echo grid_get_template_part('tpl/forms', 'select' . ((isset($info['format']) && !empty($info['format'])) ? '-'.$info['format'] : ''), $args);
        break;

        case 'textarea' :
          echo grid_get_template_part('tpl/forms', 'textarea' . ((isset($info['format']) && !empty($info['format'])) ? '-'.$info['format'] : ''), $args);
        break;

        case 'upload' :
          echo grid_get_template_part('tpl/forms', 'input-upload' . ((isset($info['format']) && !empty($info['format'])) ? '-'.$info['format'] : ''), $args);
        break;

      endswitch;


      if(isset($args['posttext'])){
        echo $args['posttext'];
      }
    endforeach;
    ?>



    <input type="hidden" name="empty-input" value=""/>
		<input type="hidden" name="submit-grid_manage_<?php echo $form->name;?>" value="1"/>

    <div class="form--submit">
      <button class="button--xxl blue has-icon" type="submit">
        <span class="label"><?php echo __('Valider', 'grid_lang');?></span>
        <svg class="icon icon-arrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow"></use></svg>
      </button>
    </div>

</form>

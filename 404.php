<?php get_header(); ?> 

<div id="page404" style="padding: 40px;text-align:center;">
    
    <p class="h1">404 : Page introuvable</p>

    <div>
        <p>La page que vous souhaitez consulter n'existe pas ou n'existe plus.</p>
    </div>
        
</div>

<?php get_footer(); ?>

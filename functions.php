<?php

// Prevent bad stuff with magic quotes
/*if ( !is_admin() && get_magic_quotes_gpc() ) {
    $_POST      = array_map( 'stripslashes_deep', $_POST );
    $_GET       = array_map( 'stripslashes_deep', $_GET );
    $_COOKIE    = array_map( 'stripslashes_deep', $_COOKIE );
    $_REQUEST   = array_map( 'stripslashes_deep', $_REQUEST );
}*/

$host = (isset( $_SERVER['HTTP_HOST'] )) ? $_SERVER['HTTP_HOST'] : '';

// THEME INFOS
// global $grid_superadmin_ids;
// $grid_superadmin_ids = array(1, 2); // IDs des admins qui auronts toutes les fonctionnalités du BO
define ('GRID_JPEG_QUALITY', 90);
define ('GRID_IS_LOCAL', ( strpos( $host, '.local' ) !== FALSE ));

// THEME PARAMS
define('ACTIVE_NEWSLETTER', false); // ajouter le module de register newsletter ou non
define('MAILCHIMP_APIKEY', ''); // si ACTIVE_NEWSLETTER=true et enregistrement des mails dans MAILCHIMP
define('MAILCHIMP_LISTID', ''); // si ACTIVE_NEWSLETTER=true et enregistrement des mails dans MAILCHIMP
define('ACTIVE_COMMENTS', true); // active les commentaires par défaut ou non
define('ACTIVE_POSTTYPEPOST', false); // désactiver le post type "post" ou non

// FUNCTIONS
include( get_parent_theme_file_path() . '/incs/functions/functions.php' );
include( get_parent_theme_file_path() . '/incs/functions/actions.php' );
include( get_parent_theme_file_path() . '/incs/functions/filters.php' );
include( get_parent_theme_file_path() . '/incs/functions/post_types.php' );
include( get_parent_theme_file_path() . '/incs/functions/taxonomies.php' );
include( get_parent_theme_file_path() . '/incs/functions/menus.php' );


// THEME ACTIVATION
include( get_parent_theme_file_path() . '/incs/activation.php' );


// BO
include( get_parent_theme_file_path() . '/incs/back-office/reset.php' );
include( get_parent_theme_file_path() . '/incs/back-office/protected.php' );
include( get_parent_theme_file_path() . '/incs/back-office/gutenberg.php' );
include( get_parent_theme_file_path() . '/incs/back-office/pimp-my-bo.php' );
include( get_parent_theme_file_path() . '/incs/back-office/columns.php' );

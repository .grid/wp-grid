<?php
add_action( 'after_setup_theme', 'grid_theme_setup' );
function grid_theme_setup(){
  // Update main options
  $options_default = array(
    // Misc configs
    'use_smilies' => '',
    'ping_sites' => 'http://rpc.pingomatic.com/'."\n".'http://blogsearch.google.com/ping/RPC2',
    'disallowed_keys' => "д\nи\nж\nЧ\nБ\nЏ\nЂ\nћ\nР°\nЃ",
    // Dates
    'date_format' => 'j F Y',
    'timezone_string' => 'Europe/Paris',
    'links_updated_date_format' => 'j F Y, G \h i \m\i\n',
    // Permalink Structure
    'permalink_structure' => '/%category%/%postname%/',
    // Medias size
    'thumbnail_size_w' => '50',
    'thumbnail_size_h' => '50',
    'thumbnail_crop' => '1',
    'medium_size_w' => '1000',
    'medium_size_h' => '800',
    'large_size_w' => '1440',
    'large_size_h' => '1000',
  );
  foreach ($options_default as $k => $v) :
      update_option($k, $v);
  endforeach;


  // Avoid 404 permalinks
  flush_rewrite_rules();

}

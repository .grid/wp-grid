<?php

// Désactive les widgets par défaut dans l'admin Wordpress
add_action('widgets_init', 'unregister_default_wp_widgets');
function unregister_default_wp_widgets() {
//    unregister_widget('WP_Widget_Categories');
//    unregister_widget('WP_Widget_Recent_Posts');
//    unregister_widget('WP_Widget_Search');
//    unregister_widget('WP_Widget_Tag_Cloud');
//    unregister_widget('WP_Widget_Meta');
//    unregister_widget('WP_Widget_Pages');
//    unregister_widget('WP_Widget_Calendar');
//    unregister_widget('WP_Widget_Archives');
//    unregister_widget('WP_Widget_Links');
//    unregister_widget('WP_Widget_Recent_Comments');
//    unregister_widget('WP_Widget_RSS');
//    unregister_widget('WP_Widget_Text');
//    unregister_widget('WP_Nav_Menu_Widget');
}

// Suppression des Widgets inutiles dans le Dashboard
add_action('wp_dashboard_setup', 'grid_remove_dashboard_widgets');
function grid_remove_dashboard_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}


// Supprime des menus inutiles dans l'admin Wordpress
add_action('admin_menu', 'remove_menus');
function remove_menus() {
    global $menu;
    $restricted = array();
    // $restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
    end($menu);
    while (prev($menu)) {
        $value = explode(' ', $menu[key($menu)][0]);
        if (in_array($value[0] != NULL ? $value[0] : "", $restricted)) {
            unset($menu[key($menu)]);
        }
    }
}

// Supprime le script d'internationalisation : non utilisé par défaut.
if (!is_admin()) {
    add_action('init', 'remove_l1on');
    function remove_l1on() {
        wp_deregister_script('l10n');
    }
}


// Supprime la version de Wordpress affichée dans le head
//remove_action('wp_head', 'wp_generator');

// Disable auto update
// see http://codex.wordpress.org/Disabling_Automatic_Background_Updates#All_Updates
function grid_automatic_updater_disabled() {
    return true;
}
add_filter( 'automatic_updater_disabled', 'grid_automatic_updater_disabled' );

<?php

add_filter( 'allowed_block_types', 'tf_allowed_gutenberg_blocks');
function tf_allowed_gutenberg_blocks( $allowed_block_types ) {
    return array(

        'core/heading', // titre
        'core/image', // image
        //'core/gallery', // gallery
        //'core/quote', // quote
        'core/list', // list
        'core/paragraph', // paragraph
        //'core/video', // video
        //'core/audio', // sound

        //'core/pullquote',
        //'core/table',
        'core/code',
        //'core/preformatted',
        //'core/freeform', // classic editor
        //'core/verse',
        'core/html',

        //'core/separator',
        //'core/more',
        //'core/button',

        //'core/shortcode',
        //'core/categories',
        //'core/latest-posts',

        //'core/embed',

        //'core/cover-image',
        //'core/text-columns',
        //'core/block',

        //'core-embed/youtube',
        //'core-embed/facebook',
        //'core-embed/instagram',
        //'core-embed/wordpress',
        //'core-embed/soundcloud',
        //'core-embed/spotify',
        //'core-embed/flickr',
        //'core-embed/vimeo',
        //'core-embed/animoto',
        //'core-embed/cloudup',
        //'core-embed/collegehumor',
        //'core-embed/dailymotion',
        //'core-embed/funnyordie',
        //'core-embed/hulu',
        //'core-embed/imgur',
        //'core-embed/issuu',
        //'core-embed/kickstarter',
        //'core-embed/meetup-com',
        //'core-embed/mixcloud',
        //'core-embed/photobucket',
        //'core-embed/polldaddy',
        //'core-embed/reddit',
        //'core-embed/reverbnation',
        //'core-embed/screencast',
        //'core-embed/scribd',
        //'core-embed/slideshare',
        //'core-embed/smugmug',
        //'core-embed/speaker',
        //'core-embed/ted',
        //'core-embed/tumblr',
        //'core-embed/videopress',
        //'core-embed/wordpress-tv'
    );
}


function tf_gutenberg_blocks() {

    wp_enqueue_script(
        'gutenblocks-block-deactivator',
        untrailingslashit(get_stylesheet_directory_uri()) . '/incs/admin/js/deactivator.js',
        [ 'wp-blocks', 'wp-i18n', 'wp-element' ],
        '1.0',
        true
    );

}
//add_action( 'enqueue_block_editor_assets', 'tf_gutenberg_blocks' );

<?php

// Change le logo du login
//add_action('login_head', 'grid_custom_login_logo');
function grid_custom_login_logo() {
    echo '<style type="text/css">
        .login h1 a { background-image:url('.get_bloginfo('template_directory').'/medias/images/logo.png) !important;background-size:contain!important;width:100%; }
    </style>';
}

// Change login page logo URL
add_filter("login_headerurl", "grid_custom_login_link");
function grid_custom_login_link($url) {
    return site_url();
}

// Change login page logo txt
add_filter("login_headertitle", "grid_custom_login_title");
function grid_custom_login_title($message) {
    return get_bloginfo('name');
}

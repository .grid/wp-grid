<?php
add_filter('manage_posts_columns', 'grid_column_bo_image_th');
function grid_column_bo_image_th($columns) {
    $columns['grid_image'] = 'Image';
    return $columns;
}

add_action('manage_posts_custom_column', 'grid_column_bo_image_td', 10, 2);
function grid_column_bo_image_td($column_name, $post_ID) {
    global $post;
    if (function_exists('get_field') && $column_name == 'grid_image') {
        $img = (int)get_field('thumb');
        if($img>0)
          echo '<img src="' . grid_get_image_url($img, 'full') . '" style="max-height:50px;width:auto;" />';
    }
}

<?php


// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_false');
}



if(is_admin()){

    // On retire l'accès aux bas-niveaux d'users à WP-ADMIN
    add_action('admin_init', 'grid_admin_init');
    function grid_admin_init() {
        global $userdata;
        if (!current_user_can('edit_posts') && !DOING_AJAX) {
            wp_redirect('/');
            die;
        }
    }

    // Empeche le client de switcher de thèmes
    add_action('admin_init', 'grid_cwc_lock_theme');
    function grid_cwc_lock_theme() {
        global $submenu, $userdata, $grid_superadmin_ids;
        if (isset($userdata->ID) && !in_array($userdata->ID, $grid_superadmin_ids)) {
            unset($submenu['themes.php'][5]);
            unset($submenu['themes.php'][15]);
        }
    }

    // On empeche l'édition de fichiers du thème & des plugins
    add_action('init', 'grid_init_constantes');
    function grid_init_constantes() {
        global $userdata, $grid_superadmin_ids;
        if (isset($userdata->ID) && !in_array($userdata->ID, $grid_superadmin_ids)) {
            if (!defined('DISALLOW_FILE_EDIT'))
                define('DISALLOW_FILE_EDIT', true);
            if (!defined('DISALLOW_FILE_MODS'))
                define('DISALLOW_FILE_MODS', true);
        }
    }

    // Supprime l'update nag
    add_action('init', 'grid_remove_update_nag');
    function grid_remove_update_nag() {
        global $userdata, $grid_superadmin_ids;
        if (isset($userdata->ID) && !in_array($userdata->ID, $grid_superadmin_ids)) {
            add_action('init', 'grid_remove_action_wp_check', 2);
            add_filter('pre_option_update_core', 'grid_pre_option_update_core');
        }
    }

		function grid_remove_action_wp_check(){
			remove_action( 'init', 'wp_version_check' );
		}

		function grid_pre_option_update_core(){
			return null;
		}

    add_action('admin_head','grid_remove_button_version_message');
    function grid_remove_button_version_message() {
        global $userdata, $grid_superadmin_ids;
        if (isset($userdata->ID) && !in_array($userdata->ID, $grid_superadmin_ids)) {
            echo '<style>#wp-version-message{display:none}</style>';
        }
    }

    // Empeche le client d'accéder au menu d'update nag
    add_action('admin_menu', 'grid_remove_submenus');
    function grid_remove_submenus() {
        global $submenu, $userdata, $grid_superadmin_ids;
        if (isset($userdata->ID) && !in_array($userdata->ID, $grid_superadmin_ids)) {
            unset($submenu['index.php'][10]);
            remove_action( 'admin_notices', 'update_nag', 3 );
            if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], 'update-core.php')) {
                wp_redirect(site_url() . '/wp-admin/');
                die;
            }
        }
    }
}

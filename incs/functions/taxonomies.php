<?php
// Creation des taxos
function grid_create_taxonomies($grid_taxonomies){
    foreach($grid_taxonomies as $slug => $args){

        if(!isset($args['hierarchical']) || !is_bool($args['hierarchical']))
            $args['hierarchical'] = true;
        if(!isset($args['feminin']) || !is_bool($args['hierarchical']))
            $args['feminin'] = true;
        if(empty($args['post_types']))
            $args['post_types'] = array('post','page');
        if(is_string($args['post_types']))
            $args['post_types'] = array($args['post_types']);

        $un_fem = 'un'.($args['feminin'] ? 'e':'').' '.$args['name'];

        register_taxonomy($slug,$args['post_types'], array(
            'hierarchical' => $args['hierarchical'],
            'labels' => array(
            'name' => _x( $args['pluriel'], 'taxonomy general name' ),
            'singular_name' => _x( $args['name'], 'taxonomy singular name' ),
            'search_items' =>  ( 'Search '.$args['pluriel'] ),
            'all_items' => ( 'All '.$args['pluriel'] ),
            'parent_item' => ( 'Parent '.$args['name'] ),
            'parent_item_colon' => ( 'Parent '.$args['name'].':' ),
            'edit_item' => ( 'Modifier '.$un_fem ),
            'update_item' => ( 'Mettre &agrave; jour '.$un_fem ),
            'add_new_item' => ( 'Ajouter '.$un_fem ),
            'new_item_name' => ( 'Nouve'.($args['feminin'] ? 'lle':'au').' nom de '.$args['name'].' ' ),
            'menu_name' => ( $args['pluriel'] ),
        ),
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => $slug ),
        ));
    }
}

<?php
/**
 * Tronque le contenu à un certain nombre de caracteres
 * @param string    $string        Chaine à limiter
 * @param int       $width         Nombre de caractères minimum
 * @param string    $pad           Optionnel. Chaine à rajouter à la fin, type "...".
 * @return string
 */
function truncated($string, $width, $pad="...") {
    if(strlen($string) > $width) {
        $string = str_replace("\n",' ',$string);
        $string = wordwrap($string, $width);
        $string = substr($string, 0, strpos($string, "\n")). $pad;
    }
    return $string;
}

/**
 * Récupère la date au format "Il y a ... jours"
 * @param int    $timestamp        Timestamp
 * @return string
 */
function get_relative_time($timestamp){
	$time = '';
	$time_diff = current_time('timestamp') - $timestamp;
	$unit = '';
	$value = '';

	if($time_diff > 60 * 60 * 24 * 365){
		$unit = __('an', 'grid_lang');
		$value = round($time_diff/(60*60*24*365));
	} elseif ($time_diff > 60 * 60 * 24 * 30){
		$unit = __('mois', 'grid_lang');
		$value = round($time_diff/(60*60*24*30));
	} elseif ($time_diff > 60 * 60 * 24 * 7){
		$unit = __('semaine', 'grid_lang');
		$value = round($time_diff/(60*60*24*7));
	} elseif ($time_diff > 60 * 60 * 24){
		$unit = __('jour', 'grid_lang');
		$value = round($time_diff/(60*60*24));
	} elseif ($time_diff > 60 * 60){
		$unit = __('heure', 'grid_lang');
		$value = round($time_diff/(60*60));
	} elseif ($time_diff > 60){
		$unit = __('minute', 'grid_lang');
		$value = round($time_diff/60);
	} else {
		$unit = __('seconde','grid_lang');
		$value = $time_diff;
	}

	$final_unit = ( $unit!='mois' && $value>1 ) ? 's' : '';

	return sprintf(__("Il y a %d %s%s",'grid_lang'), $value, $unit, $final_unit);
}

/**
 * Génère l'URL de l'image
 * @param int    $image        Image (id ou array)
 * @param string    $size        Taille de l'image
 * @return string
 */
function grid_get_image_url($image, $size = 'thumbnail'){
      $urlimg = get_bloginfo('stylesheet_directory') . '/medias/images/defaults/' . $size . '.png';
      if(is_array($image) && isset($image['ID']) && isset($image['sizes'][$size])){
          return $image['sizes'][$size];
      }elseif((int)$image>0) {
          $urlimgarray = wp_get_attachment_image_src($image,  $size );
          if ($urlimgarray != false) {
              $urlimg = $urlimgarray[0];
              return $urlimg;
          }
      } else {
          return $urlimg;
      }

      return '';
  }

function grid_get_image_size_url($img_full_url, $size) {
    global $grid_thumbs_sizes;

    if(empty($size) || $size == 'full' || $size == 'medium')
        return $img_full_url;

    $height = (!empty($grid_thumbs_sizes[$size]['height'])) ? $grid_thumbs_sizes[$size]['height'] : 0;
    $width = (!empty($grid_thumbs_sizes[$size]['width'])) ? $grid_thumbs_sizes[$size]['width'] : 0;
    $suffix = "{$width}x{$height}";
    $info = pathinfo($img_full_url);
    $dir = $info['dirname'];
    $ext = $info['extension'];
    $name = basename($img_full_url, ".{$ext}");
    $destfilename = "{$dir}/{$name}-{$suffix}.{$ext}";
    return $destfilename;
}


function grid_sendmail( $destinataires = array(), $modele = 'contact', $sujet = '', $donnees = array(), $frommail = '', $fromname = ''){

    if($frommail=='')
        $frommail = get_bloginfo('admin_email');

    if($fromname=='')
        $fromname = get_bloginfo('name');

    $mail_content = '';
    if(!empty($modele) && preg_match('#^([a-z0-9_-]+)$#', $modele) && file_exists( TEMPLATEPATH.'/mails/model-'.$modele.'.php' )) {
        ob_start();
        include TEMPLATEPATH.'/mails/model-'.$modele.'.php';
        $mail_content = ob_get_clean();
    }

    ob_start();
    include TEMPLATEPATH.'/mails/header.php';
    echo $mail_content;
    include TEMPLATEPATH.'/mails/footer.php';
    $body = ob_get_clean();

    $headers = array();
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'From: ' . $fromname . ' <' . $frommail . '>';
//        $headers[] = 'Cc: John Q Codex <jqc@wordpress.org>';
//        $headers[] = 'Cc: iluvwp@wordpress.org'; // note you can just use a simple email address

    return wp_mail( $destinataires, stripslashes(utf8_decode('['.get_option('blogname').'] ' . $sujet)), $body, $headers );
}


/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param    array  $plugins
 * @return   array  Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}



function grid_get_template_part($group_template, $file, $args = array())
{
    global $wpdb,$post;
    $retour = false;
    $filename = get_theme_file_path().'/'.$group_template.'/'.$file.'.php';
    $cache_filename = (!empty($args['filename'])) ? $args['filename'] : '';
    $cache_file = (($cache_filename!='') ? sanitize_title($cache_filename).'-' : '').md5($filename);
    $cache_dir = get_theme_file_path().'/wp-content/grid_cache/';

    $cache_valide = false;

    if (isset($args['expires']) && (int)$args['expires'] > 0) {
        $cache_valide = true;
        if (GRID_IS_LOCAL || WP_DEBUG) {
            $cache_valide = false;
        } elseif (!isset($args['expires']) || $args['expires'] == 0 || WP_DEBUG) {
            // Doit-on utiliser le cache ?
            $cache_valide = false;
        } elseif (isset($_GET['clear_cache_grid']) && current_user_can('manage_options')) {
            // on vide les caches
            $cache_valide = false;
        } elseif ($cache_valide && (!file_exists($cache_dir . $cache_file))) {
            // Le cache est-il valide ?
            $cache_valide = false;
        }
        // Le fichier de cache est-il expiré ?
        if ($cache_valide && filemtime($cache_dir . $cache_file) + $args['expires'] < time()) {
            $cache_valide = false;
        }
    }
    // On recupere le fichier demandé
    ob_start();
    if ($cache_valide) {
        include $cache_dir.$cache_file;
    } elseif (file_exists($filename)) {
        include $filename;
    }
    $retour .= ob_get_contents();
    ob_end_clean();
    if (!$cache_valide && isset($args['expires']) && (int)$args['expires'] > 0) {
        $cache_dir = $grid_cachebloc->cache_dir;
        if (!is_dir($cache_dir)) {
            mkdir($cache_dir);
            @chmod($cache_dir, 0777);
        }
        $file_create = file_put_contents($cache_dir.$cache_file, $retour);
    }

    return $retour;
}

function grid_removep($text){
  return strip_tags($text, '<br>,<a>,<span>,<b>,<i>,<strong>,<ul>,<li>');
}

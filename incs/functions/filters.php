<?php
// Filenames
add_filter('sanitize_file_name', 'remove_accents' );
add_filter('sanitize_file_name', 'strtolower' );

// JPG Quality
add_filter('jpeg_quality', 'grid_jpeg_quality_callback');
function grid_jpeg_quality_callback($arg) {
    if (!defined('GRID_JPEG_QUALITY'))
        define('GRID_JPEG_QUALITY', 90);
    return (int) GRID_JPEG_QUALITY;
}

// fix invalid links
add_filter( 'the_content', 'grid_fix_content_href' );
function grid_fix_content_href( $content ) {
    $content = str_replace( 'href="www.', 'href="http://www.', $content );
    $content = str_replace( 'href="http//.', 'href="http://', $content );
    return $content;
}

//Upscale media
add_filter('image_resize_dimensions', 'grid_image_crop_dimensions', 10, 6);
function grid_image_crop_dimensions($default, $orig_w, $orig_h, $new_w, $new_h, $crop){
    if ( !$crop )
      return null; // let the wordpress default function handle this

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);

    $s_x = floor( ($orig_w - $crop_w) / 2 );
    $s_y = floor( ($orig_h - $crop_h) / 2 );

    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}

// disable emojis everywhere
add_action( 'init', 'disable_emojis' );
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}

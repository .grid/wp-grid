<?php

// Authorized Users
define('ACF_ADMINS', array(
    'developer@grid-agency.com'
));

add_filter('acf/settings/show_admin', 'grid_show_acf_admin');
function grid_show_acf_admin( $show ) {
    $currentUser = wp_get_current_user();
    return in_array($currentUser->user_email, ACF_ADMINS);
}

//add_filter('acf/load_field/name=header_color', 'grid_default_select');
function grid_default_list_colors( $field ) {
    $field['choices'] = array(
    );
    return $field;
}


//add_action('acf/init', 'grid_acf_options');
function grid_acf_options() {
    if( function_exists('acf_add_options_page') ) {
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Options'),
            'menu_title'    => __('Options'),
            'capability'    => 'edit_posts',
            'icon_url'     => 'data:image/svg+xml;base64,' . base64_encode( '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.5 3.5L16 7l-3.5-3.5L0 16l12.5 12.5L16 25l3.5 3.5L32 16 19.5 3.5zM16 23.3L8.7 16 16 8.7l7.3 7.3-7.3 7.3zm-3.5 3.5L1.7 16 12.5 5.2l2.6 2.6L7 16l8.1 8.1-2.6 2.7zm4.4-2.7L25 16l-8.1-8.1 2.6-2.6L30.3 16 19.5 26.8l-2.6-2.7z" fill="#fff"/></svg>' )
        ));

        // Add Networks subpage.
        $networks = acf_add_options_page(array(
            'page_title'  => __('Réseaux Sociaux'),
            'menu_title'  => __('Réseaux Sociaux'),
            'parent_slug' => $option_page['menu_slug'],
            'menu_slug'     => 'networks-options',
        ));
    }
}


add_action('acf/input/admin_head', 'grid_acf_wysiwyg_height');
function grid_acf_wysiwyg_height(){
    ?>
    <style type="text/css">
        .wp-editor-wrap[data-toolbar="title_wysiwyg"] iframe{
            height: 100px !important;
            min-height: 100px !important;
        }
    </style>
    <?php
}


add_filter('acf/fields/wysiwyg/toolbars', 'grid_wysiwyg_acf');
function grid_wysiwyg_acf($toolbars){
    $toolbars['Title Wysiwyg'] = array();
    //list buttons : https://www.tiny.cloud/docs-4x/advanced/editor-control-identifiers/#toolbarcontrols
    $toolbars['Title Wysiwyg'][1] = array('forecolor', 'alignleft', 'aligncenter', 'alignright' );

    return $toolbars;
}

//add_filter('tiny_mce_before_init', 'grid_custom_color_wysiwyg');
function grid_custom_color_wysiwyg($init){
    $custom_colours = '
        "EA168C", "Rose",
        "222222", "Noir"
    ';

    $init['textcolor_map'] = '['.$custom_colours.']';
    $init['textcolor_rows'] = 1;

    return $init;
}

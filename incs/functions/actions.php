<?php
// hook to create admin user quickly
//add_action('init', 'grid_create_admin_user');
function grid_create_admin_user(){
    $newusername = 'YOURUSERNAME';
    $newpassword = 'YOURPASSWORD';
    $newemail = 'YOUREMAIL@TEST.com';
    if ( $newpassword != 'YOURPASSWORD' && $newemail != 'YOUREMAIL@TEST.com' && $newusername !='YOURUSERNAME' ){
        if ( !username_exists($newusername) && !email_exists($newemail) ){
            $user_id = wp_create_user( $newusername, $newpassword, $newemail);
            if ( is_int($user_id) ){
                $wp_user_object = new WP_User($user_id);
                $wp_user_object->set_role('administrator');
            }
        }
    }
}

// Find translations in "/lang/" folder
add_action( 'init', 'grid_lang_init' );
function grid_lang_init() {
    load_theme_textdomain( 'grid_lang', untrailingslashit(get_template_directory()) . '/lang' );
}

// Cancel pings for intern links
add_action( 'pre_ping', 'disable_self_ping' );
function disable_self_ping( &$links ) {
    foreach ( $links as $l => $link )
        if ( 0 === strpos( $link, site_url() ) )
            unset( $links[$l] );
}

// generate “/acf-json/" folder if not exists
add_action( 'init', 'grid_acf_json_dir' );
function grid_acf_json_dir(){
    $path = get_parent_theme_file_path().'/acf-json/';
    if(!is_dir($path))
        mkdir($path);
}

<?php
// Creation des post-types
function grid_create_posttypes($grid_post_types){

    foreach($grid_post_types as $idpt => $pt){
        if(!isset($pt['pluriel'])) $pt['pluriel'] = $idpt;
        if(!isset($pt['singulier'])) $pt['singulier'] = $idpt;
        if(!isset($pt['feminin'])) $pt['feminin'] = 0;
        if(!isset($pt['public'])) $pt['public'] = true;
        if(!isset($pt['publicly_queryable'])) $pt['publicly_queryable'] = true;
        if(!isset($pt['exclude_from_search'])) $pt['exclude_from_search'] = false;
        if(!isset($pt['hierarchical'])) $pt['hierarchical'] = false;
        if(!isset($pt['supports'])) $pt['supports'] = array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes');
        if(!isset($pt['taxonomies'])) $pt['taxonomies'] = array('post_tag','category');
        $rewrite = (isset($pt['rewrite'])) ? $pt['rewrite'] : $idpt;
        if(!isset($pt['has_archive'])) $pt['has_archive'] = $rewrite;

        $fem_single = ($pt['feminin'] ? 'e':'');
        $article_single = 'un'.$fem_single;
        $new_single = 'nouve'.($pt['feminin'] ? 'lle':'au');
        $no_single = 'aucun'.$fem_single;

        $_PT_PARAMS = array(
            'label' => ucfirst($pt['pluriel']),
            'singular_label' => ucfirst($pt['singulier']),
            'description' => ucfirst($pt['singulier']),
            'public' => $pt['public'],
            'publicly_queryable' => $pt['publicly_queryable'],
            'exclude_from_search' =>  $pt['exclude_from_search'],
            'taxonomies' => $pt['taxonomies'],
            'menu_position' => 5,
            'show_ui' => true,
            '_builtin' => false,
            'show_in_menu' => true,
            'hierarchical' => $pt['hierarchical'],
            'query_var' => true,
            'has_archive' => $pt['has_archive'],
            'rewrite' => array('slug' => $rewrite),
            'supports' => $pt['supports'],
            'capability_type'=>(isset($pt['capability_type'])) ? $pt['capability_type'] : 'post',
            'labels' => array (
                'name' => ucfirst($pt['pluriel']),
                'name_admin_bar' =>  ucfirst($pt['singulier']),
                'singular_name' => ucfirst($pt['singulier']),
                'menu_name' => ucfirst($pt['pluriel']),
                'add_new' => 'Ajouter '.$article_single.' '.$pt['singulier'],
                'add_new_item' => 'Ajouter '.$article_single.' '.$new_single.' '.$pt['singulier'],
                'edit' => 'Modifier',
                'edit_item' => 'Modifier '.$article_single.' '.$pt['singulier'],
                'new_item' => ucfirst($new_single).' '.$pt['singulier'],
                'view' => 'Voir des '.$pt['pluriel'],
                'view_item' => 'Voir '.$article_single.' '.$pt['singulier'],
                'search_items' => 'Rechercher dans les '.$pt['pluriel'],
                'not_found' => ucfirst($no_single).' '.$pt['singulier'].' trouv&eacute;'.$fem_single,
                'not_found_in_trash' => ucfirst($no_single).' '.$pt['singulier'].' trouv&eacute;'.$fem_single.' dans la corbeille',
                'parent' => ucfirst($pt['pluriel']).' parent'.$fem_single.'s',
            ),
        );

        if(isset($pt['capabilities']))
            $_PT_PARAMS['capabilities'] = $pt['capabilities'];
        if(isset($pt['menu_icon']))
            $_PT_PARAMS['menu_icon'] = $pt['menu_icon'];

        register_post_type($idpt,$_PT_PARAMS);
    }
}

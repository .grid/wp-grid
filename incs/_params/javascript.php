<?php

add_action( 'wp_enqueue_scripts', 'grid_load_js' );
function grid_load_js() {
  if ( !is_admin() ) {

    $js_dir = untrailingslashit(get_template_directory_uri()) . '/js/';
    $in_footer = true;

    wp_register_script( 'theme-js', $js_dir . 'scripts.min.js', 'jquery', '1.0', $in_footer );

    // send dynamic infos to js file
    $themesInfos = array(
      "wp_ajax_url" => admin_url('admin-ajax.php'),
      "wp_template_url" => untrailingslashit(get_template_directory_uri()),
      "wp_site_url" => site_url(),
    );
    wp_localize_script( 'theme-js', 'theme_infos', $themesInfos );

    wp_deregister_script('jquery');
    wp_register_script('jquery', ("//code.jquery.com/jquery-3.3.1.min.js"), array(), '3.3.1', false);
    wp_enqueue_script('jquery');

    wp_enqueue_script( 'theme-js' );
  }

}

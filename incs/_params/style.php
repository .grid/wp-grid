<?php

add_action( 'wp_enqueue_scripts', 'grid_load_css' );
function grid_load_css() {

	wp_enqueue_style( 'theme-style', untrailingslashit(get_stylesheet_directory_uri()).'/style.min.css', false );

}

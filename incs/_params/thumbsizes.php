<?php
global $grid_thumbs_sizes;
$grid_thumbs_sizes = array(
    /*'300x600_example' => array(
        'width' => 300,
        'height' => 600,
        'crop' => true,
        'name' => 'Thumb size example 300x600'
    ),*/
);


/**
 * Don't touch
 */

// automatic add_image_size
global $addsizes;
$addsizes = array();
foreach ($grid_thumbs_sizes as $slug => $args) {
    add_image_size( $slug, $args['width'], $args['height'], $args['crop'] );
    if( isset($args['name']) ) {
        $addsizes[$slug] = $args['name'];
    }
}

// For Media Library Images (Admin)
// https://developer.wordpress.org/reference/functions/add_image_size/
add_filter( 'image_size_names_choose', 'grid_custom_sizes' );
function grid_custom_sizes( $sizes ) {
    global $addsizes;
    $newsizes = array_merge($sizes, $addsizes);
    return $newsizes;
}

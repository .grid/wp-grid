<?php
add_action( 'init', 'grid_register_taxonomies', 0 );
function grid_register_taxonomies() {
    global $grid_taxonomies;
    $grid_taxonomies = array(
         /*'example' => array(
             'name' => 'Example',
             'pluriel' => 'Examples',
             'feminin' => 0,
             'hierarchical' => true, // true = category, false = post_tag
             'post_types' => array( 'post' ), // post types
         ),*/
    );
    grid_create_taxonomies( $grid_taxonomies );

}

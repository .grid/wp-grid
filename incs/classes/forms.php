<?php
class grid_forms {

    var $accuseReception = false;

    function __construct() {

        global $wpdb;


        //if(get_parent_class($this)){

        $this->slug = 'grid_manage_'.$this->name;
        $this->tablename = $wpdb->prefix . 'grid_register_'.$this->name;

        add_action('init', array(&$this, 'grid_export_csv'));

        add_action('wp_ajax_nopriv_submit_'.$this->name, array(&$this, 'save_form'));
        add_action('wp_ajax_submit_'.$this->name, array(&$this, 'save_form'));

        add_action('admin_init', array(&$this, 'check_updates'));

        add_action('admin_menu', array(&$this, 'init_box'));

    }

    function check_updates(){
        if(isset($this->version)){
            $prevVersion = get_option( "grid_register_".$this->name."_db_version" );
            if ( $this->version != $prevVersion ) {
                $this->init_tables();
            }
        }
    }

    function init_tables() {

        global $wpdb;

        $fields = $this->getFields();
        if(count($fields)>0){

            $sql = 'CREATE TABLE `' . $this->tablename . '` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `date` datetime DEFAULT NULL,';

            foreach($this->getFields() as $key=>$values):
                $sqlvalue = (!isset($values['sqlinfo']) || $values['sqlinfo']=='') ? 'text' : $values['sqlinfo'];
                $sql .= '
                `' . $key . '` ' . $sqlvalue . ',';
            endforeach;

            $sql .= '
              PRIMARY KEY  (`id`)
);';
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta($sql);
        }

        update_option( "grid_register_".$this->name."_db_version", $this->version );
    }

    function init_box() {
        $pages[] = add_menu_page($this->label, $this->label, 'level_5', $this->slug, array (&$this, 'manage' ), ((isset($this->formDashicon)) ? $this->formDashicon : '') );
    }

    function grid_export_csv() {
        global $wpdb;

        if (isset($_GET['download']) && current_user_can('manage_options') && isset($_GET['page']) && $_GET['page']==$this->slug) {
            set_time_limit(0);
            header("Content-Type: application/csv-tab-delimited-table");
            header("Content-disposition: filename=export_".$this->name."_".date('Ymd').".csv");
            $init = (isset($_POST['initdwld']) && (int)$_POST['initdwld']>0) ? $_POST['initdwld'] : 0;
            $limit = (isset($_POST['nbdwld']) && (int)$_POST['nbdwld']>0) ? $_POST['nbdwld'] : 1000;

            $request = 'SELECT * FROM '.$this->tablename.' WHERE 1=1 ';

            if(isset($_POST['when'])){
                switch($_POST['when']){
                    case '1mois':
                        $request .= ' AND date >= DATE_SUB(NOW(), INTERVAL 1 MONTH) ';
                        break;
                    case '2mois':
                        $request .= ' AND date >= DATE_SUB(NOW(), INTERVAL 2 MONTH) ';
                        break;
                    case '6mois':
                        $request .= ' AND date >= DATE_SUB(NOW(), INTERVAL 6 MONTH) ';
                        break;
                    case '1an':
                        $request .= ' AND date >= DATE_SUB(NOW(), INTERVAL 1 YEAR) ';
                        break;
                    case '2ans':
                        $request .= ' AND date >= DATE_SUB(NOW(), INTERVAL 2 YEAR) ';
                        break;
                }
            }

            $request .= ' ORDER BY date DESC';
            $myrows = $wpdb->get_results($request, ARRAY_A);
            foreach($myrows as $data){
                echo implode( ';', $data ) . "\r\n";
            }
            die;
        }
    }


    function manage($post) {
        global $wpdb;
        $fields = $this->getFields();
        $columns = array();
        foreach($fields as $field=>$infos)
            if(isset($infos['backoffice']) && $infos['backoffice']==true)
                $columns[$field] = $infos['label'];
        ?>

        <div class="export-results">

            <h3><?php echo $this->label;?> - Export</h3>

            <p style="padding:15px 0;">
                <a href="<?php echo admin_url('admin.php?page='.$this->slug.'&download');?>" class="button-primary">Télécharger la liste complète (.csv)</a>
            </p>

            <hr style="margin:30px 0 20px 0;"/>

            <h3 style="margin-top:25px;">Derniers enregistrements</h3>
            <div>
                <table class="widefat">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <?php foreach($columns as $key=>$label): ?>
                                <th><?php echo $label;?></th>
                            <?php endforeach;?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <?php foreach($columns as $key=>$label): ?>
                                <th><?php echo $label;?></th>
                            <?php endforeach;?>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        global $wpdb;
                        $results = $wpdb->get_results('SELECT * FROM '.$this->tablename.' ORDER BY date DESC LIMIT 0,8');
                        foreach($results as $result):
                            ?>
                            <tr>
                                <td><?php echo $result->id;?></td>
                                <td><?php echo mysql2date('d/m/Y à H:i:s', $result->date);?></td>
                                <?php foreach($columns as $key=>$label): ?>
                                    <th><?php
                                    $labelshow = $result->$key;
                                    if(is_serialized($labelshow)){
                                      $arrayLabels = unserialize($labelshow);
                                      foreach($arrayLabels as $inc => $label){
                                        if($inc > 0){
                                          echo ', ';
                                        }
                                        echo (isset($fields[$key], $fields[$key]['datas'], $fields[$key]['datas'][$label])) ? $fields[$key]['datas'][$label] : $label;
                                      }
                                    }else{
                                      echo (isset($fields[$key], $fields[$key]['datas'], $fields[$key]['datas'][$labelshow])) ? $fields[$key]['datas'][$labelshow] : $labelshow;
                                    }
                                    ?></th>
                                <?php endforeach;?>
                            </tr>
                        <?php endforeach;?>
                    </tbody>

                </table>

            </div>
            <hr style="margin:30px 0 20px 0;"/>

            <p style="padding:15px 0;">
                <a href="<?php echo admin_url('admin.php?page='.$this->slug.'&download');?>" class="button-primary">Télécharger la liste complète (.csv)</a>
            </p>



        </div>
        <?php
    }

    function check_fields($field, $value){
      $error = false;
      $errors = array();
      if(isset($field['check'])){

          foreach($field['check'] as $check){
              if($error==false){
                  switch($check){
                      case 'required':
                          if(empty($value)){
                              $errors[] = sprintf(__('Le champ "%s" est requis'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'number':
                          if ($value != '' && !ctype_digit($value)) {
                              $errors[] = sprintf(__('Le champ "%s" doit être au format numérique', 'grid_lang'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'age':
                          if ($value != '' && ctype_digit($value) && ((int)$value<10 || $value>150)) {
                              $errors[] = sprintf(__('Le champ "%s" est invalide', 'grid_lang'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'email':
                          if ($value != '' && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                              $errors[] = sprintf(__('Le champ "%s" doit être un email valide', 'grid_lang'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'name':
                          if ($value != '' && strcspn($value, '0123456789') != strlen($value)) {
                              $errors[] = sprintf(__('Le champ "%s" ne doit contenir que des lettres et des espaces', 'grid_lang'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'url':
                          if ($value != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $value)) {
                              $errors[] = sprintf(__('Le champ "%s" doit être une URL valide', 'grid_lang'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'telephone':
                          if ($value != '' && !preg_match('`[0-9]*$`', $value)){
                              $errors[] = sprintf(__('Le champ "%s" doit être un numéro de téléphone valide', 'grid_lang'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'codepostal':
                          if ($value != '' && !preg_match("/^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$/i", $value)){
                              $errors[] = sprintf(__('Le champ "%s" doit être un code postal valide', 'grid_lang'), $field['label']);
                              $error = true;
                          }
                          break;
                      case 'maxletters':
                          if ($value != '' && isset($field['maxletters']) && (int)$field['maxletters']>0 && strlen($value)>$field['maxletters']){
                              $errors[] = sprintf(__('Le champ "%s" ne doit pas dépasser %s caractères', 'grid_lang'), $field['label'], $field['maxletters']);
                              $error = true;
                          }
                          break;
                      case 'nodouble':
                        if($value != "" && count($errors) == 0){
                          $rowExists = $wpdb->get_row('SELECT ID FROM ' . $this->tablename .' WHERE ' . $key . ' = "' . $value . '"');
                          if($rowExists){
                            $errors[] = __('Cet email a déjà été enregistré', 'grid_lang');
                            $error = true;
                          }
                        }
                      break;

                  }
              }
          }
      }


      if($value!='' && isset($field['datas']) && ($field['type']=='select' || $field['type']=='radio' || ($field['type']=='checkbox' && count($field['datas']) <= 1))){
          if(!array_key_exists($value, $field['datas'])){
              $errors[] = sprintf(__('Le champ "%s" n\'est pas valable'), $field['label']);
              $error = true;
          }
      }

      if(isset($field['datas']) && $field['type']=='checkbox' && count($field['datas']) > 1 && is_array($value) && !empty($value)){
          $hasErrorCheckbox = false;
          foreach($value as $testValue){
            if($hasErrorCheckbox == false && !array_key_exists($testValue, $field['datas'])){
                $hasErrorCheckbox = true;
                $errors[] = sprintf(__('Le champ "%s" n\'est pas valable 2'), $field['label']);
                $error = true;
            }
          }
      }

      return array($error, $errors);

    }

    function save_form(){
        global $wpdb, $postValues;
        $postValues = $errors = $successes = $errorFields = array();
        $fields = $this->getFields();
        $insertdb = array('id'=>'', 'date'=>date_i18n('Y-m-d H:i:s'));

        if(isset($_POST['submit-'.$this->slug])){
            $postValues = $_POST;

            if(isset($_POST['empty-input']) && !empty($_POST['empty-input'])){
                $errors[] = __('Robot détecté');
            }

            foreach($fields as $key=>$field){
                $value = (isset($postValues[$key])) ? $postValues[$key] : '';
                $insertdb[$key] = (is_array($value)) ? serialize($value) : $value;

                $returnCheck = $this->check_fields($field, $value);
                $error = $returnCheck[0];
                $errors = $returnCheck[1];

                if($error==true)
                    $errorFields[] = $key;
            }

            // Test Akismet
            if( count($errors)==0 && function_exists( 'akismet_http_post' ) && isset($this->akismetinfos) && is_array($this->akismetinfos) && count($this->akismetinfos) == 3 ){

                if(isset($postValues[$this->akismetinfos[0]], $postValues[$this->akismetinfos[1]], $postValues[$this->akismetinfos[2]])){
                  global $akismet_api_host, $akismet_api_port;

                  $testNom = $postValues[$this->akismetinfos[0]];
                  $testEmail = $postValues[$this->akismetinfos[1]];
                  $testMessage = $postValues[$this->akismetinfos[2]];

                  $data = array(
                      'comment_author'        => $testNom,
                      'comment_author_email'  => $testEmail,
                      'comment_author_url'    => '',
                      'comment_content'       => $testMessage,
                      'user_ip'               => (isset( $_SERVER['REMOTE_ADDR'] ) ? $_SERVER['REMOTE_ADDR'] : null),
                      'user_agent'            => (isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : null),
                      'referrer'              => (isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : null),
                      'blog'                  => get_option( 'home' ),
                      'blog_lang'             => get_locale(),
                      'blog_charset'          => get_option('blog_charset'),
                      //'permalink'             => 'http://example.com/hello-world',
                      'is_test'               => TRUE,

                  );
                  $query_string = http_build_query( $data );
                  $response = akismet_http_post( $query_string, $akismet_api_host, '/1.1/comment-check', $akismet_api_port );
                  $result = ( is_array( $response ) && isset( $response[1] ) ) ? $response[1] : 'false';
                  if($result == "true"){
                      $errors[] = __('Spam détecté');
                  }
                }
            }

            if(count($errors)==0){
                $successes[] = (isset($this->succesMessage) && !empty($this->succesMessage)) ? $this->succesMessage : __("Votre message a bien été envoyé !", 'grid_lang');
                $insertdb_success = $wpdb->insert($this->tablename, $insertdb);

                if( ! $insertdb_success ) {
                    $errorFields[] = '';
                    $errors = array(__("Il y a eu une erreur au moment de l’enregistrement", 'grid_lang'));
                }

                if(count($errors)==0){
                    if(isset($this->sendEmails) && is_array($this->sendEmails) && count($this->sendEmails)>0){
                        $destinataires = $this->sendEmails;
                        $donnees = $insertdb;
                        grid_sendmail($destinataires, $this->name, __('Nouveau message - ','grid_lang').$this->label, $donnees);
                    }

                    if($this->accuseReception !== false && !empty($this->accuseReception) && isset($insertdb[$this->accuseReception]) && !empty($insertdb[$this->accuseReception]) ) {
                      grid_sendmail(array($insertdb[$this->accuseReception]), $this->name . '-client', __('Message de contact envoyé','grid_lang'), $donnees);
                    }


                    $postValues = array();
                }
            }
        }
        $return = array(
            'postValues'=>$postValues,
            'errors'=>$errors,
            'successes'=>$successes,
            'errorFields'=>$errorFields,
        );

        if(defined('DOING_AJAX') && DOING_AJAX)
            echo json_encode($return);
        else
            return $return;
        die;
    }




    function getFields(){
        return array();
    }


}

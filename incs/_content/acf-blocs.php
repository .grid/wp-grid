<?php

function grid_acf_blocks(){
  return array(
    // Attention, ne pas mettre de underscore dans les "name"
    /*array(
      'name'				=> 'text-img',
      'title'				=> __('2 colonnes Text/Image'),
      'description'		=> __(''),
      'render_callback'	=> 'grid_acf_block',
      'category'			=> 'common',
      'icon'				=> 'id',
      'keywords'			=> array( ),
    ),
    array(
      'name'				=> 'temoignage',
      'title'				=> __('Témoignage'),
      'description'		=> __(''),
      'render_callback'	=> 'grid_acf_block',
      'category'			=> 'common',
      'icon'				=> 'admin-comments',
      'keywords'			=> array( ),
    )*/
  );
}

add_action('init', 'grid_acf_blocs');
function grid_acf_blocs() {
	// check function exists
	if( function_exists('acf_register_block') ) {
    $blocks = grid_acf_blocks();
    foreach($blocks as $block){
    		acf_register_block($block);
    }
	}
}


add_filter( 'allowed_block_types', 'grid_allowed_gutenberg_blocks_acf', 11);
function grid_allowed_gutenberg_blocks_acf($allowed){
  if(is_array($allowed)){
    $blocks = grid_acf_blocks();
    foreach($blocks as $block){
      $allowed[] = "acf/".$block['name'];
    }
  }
  return $allowed;
}


function grid_acf_block( $block ) {

	$slug = str_replace('acf/', '', $block['name']);
  echo grid_get_template_part('tpl/acf-blocs', $slug);
}

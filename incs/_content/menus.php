<?php
/* AJOUT DES EMPLACEMENTS POUR LES MENUS */
add_action('init', 'grid_register_menus');
function grid_register_menus()
{

  // pour afficher dans le theme : wp_nav_menu( array( 'theme_location' => 'header' ) );

    $menus = array(
    'header' => __('Header'),
    'footer' => __('Footer'),
  );

    register_nav_menus($menus);

    if (is_admin()) {
        foreach ($menus as $menuslug => $labelmenu) {
            $menuname = $labelmenu . ' Menu';
            $menu_exists = wp_get_nav_menu_object($menuname);
            if (!$menu_exists) {
                $menu_id = wp_create_nav_menu($menuname);
                $locations = get_theme_mod('nav_menu_locations');
                $locations[$menuslug] = $menu_id;
                set_theme_mod('nav_menu_locations', $locations);
            }
        }
    }
}

/**
 * Get nav menu items by location
 *
 * @param $location The menu location id
 */
function get_nav_menu_items_by_location($location, $args = [])
{
    $locations = get_nav_menu_locations();
    $object = wp_get_nav_menu_object($locations[$location]);
    $menu_items = wp_get_nav_menu_items($object->name, $args);
    return $menu_items;
}


function clean_menu_items($items){
  $menu = array();
  $actuId = get_option('page_for_posts');

  foreach($items as $item){
    if(is_singular() && $item->type == "post_type" && $item->object_id == get_the_ID()){
      $item->current = true;
      $item->classes[] = "active";
    }elseif((is_singular('post') || is_home()) && $item->type == "post_type" && $item->object_id == $actuId){
      $item->current = true;
      $item->classes[] = "active";
    }elseif($item->type == "post_type_archive" && (is_singular($item->object) || is_post_type_archive($item->object))){
      $item->current = true;
      $item->classes[] = "active";
    }elseif(is_archive() ) {
      $currentTerm = get_queried_object();
      if($item->type == "taxonomy" && $currentTerm->taxonomy == $item->object && $currentTerm->term_id == $item->object_id){
        $item->current = true;
        $item->classes[] = "active";
      }
    }/*elseif(is_singular('post') && $item->type == "taxonomy" && $item->object == 'category'){
      if(in_category((int)$item->object_id)){
        $item->current = true;
        $item->classes[] = "active";
      }
    }*/
    $menu[$item->menu_item_parent][] = $item;
  }
  foreach($menu[0] as $inc => $itemmenu){
    if(isset($menu[$itemmenu->ID])){
      foreach($menu[$itemmenu->ID] as $submenu){
        if(isset($submenu->current) && $submenu->current === true){
          $menu[0][$inc]->current = true;
          $menu[0][$inc]->classes[] = "active";
        }
      }
    }
  }
  return $menu;
}

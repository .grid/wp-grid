<?php
add_filter( 'wp_title', 'grid_custom_title', 10, 2 );
function grid_custom_title( $title, $sep ) {
    $separator = $sep;
    if ( is_home() ) {
        $title = get_bloginfo( 'name' );
        if ( get_bloginfo( 'description' ) != '' ) {
            $title .= $separator . get_bloginfo( 'description' );
        }
    } elseif ( is_search() ) {
        $title = sprintf( __('You searched for "%s"', 'grid_lang'), get_search_query() ) . $separator . get_bloginfo( 'name' );
    } elseif ( is_404() ) {
        $title = __( 'Page non trouvée', 'grid_lang' ) . $separator . get_bloginfo( 'name' );
    } elseif ( is_single() || is_page() ) {
        $title = single_post_title( '', FALSE ) . $separator . get_bloginfo( 'name' );
    } elseif ( is_category() || is_tag() || is_tax() ) {
        $term = get_queried_object();
        if ( isset( $term->taxonomy ) ) {
            $terms_taxonomy = get_taxonomy( $term->taxonomy );
            if ( isset( $terms_taxonomy->labels->singular_name ) ) {
                $title = ucfirst($terms_taxonomy->labels->singular_name). ' : ' . single_term_title( '', false ) . $separator . get_bloginfo( 'name' );
            }
        }
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', 0) . $separator . get_bloginfo( 'name' );
    } elseif ( is_year() ) {
        $title = get_the_time( 'Y' ) . $separator . get_bloginfo( 'name' );
    } elseif ( is_month() ) {
        $title = ucfirst( get_the_time( __( 'F Y', 'grid_lang' ) ) ) . $separator . get_bloginfo( 'name' );
    } elseif ( is_day() ) {
        $title = ucfirst( get_the_time( __( 'j F Y', 'grid_lang' ) ) ) . $separator . get_bloginfo( 'name' );
    } elseif ( is_tax() ) {

    }
    return $title;
}

<?php
/*
 *
 * Se base sur le plugin disable-comments
 * https://wordpress.org/plugins/disable-comments/
 *
 */
add_action( 'widgets_init', 'grid_dc_disable_rc_widget');
add_filter( 'wp_headers', 'grid_dc_filter_wp_headers');
add_action( 'template_redirect', 'grid_dc_filter_query', 9);	// before redirect_canonical

// Admin bar filtering has to happen here since WP 3.6
add_action( 'template_redirect', 'grid_dc_filter_admin_bar');
add_action( 'admin_init', 'grid_dc_filter_admin_bar');

function grid_dc_disable_rc_widget() {
    unregister_widget( 'WP_Widget_Recent_Comments' );
}

/*
 * Remove the X-Pingback HTTP header
 */
function grid_dc_filter_wp_headers( $headers ) {
    unset( $headers['X-Pingback'] );
    return $headers;
}

/*
 * Issue a 403 for all comment feed requests.
 */
function grid_dc_filter_query() {
    if( is_comment_feed() ) {
        wp_die( __( 'Comments are closed.' ), '', array( 'response' => 403 ) );
    }
}

/*
 * Remove comment links from the admin bar.
 */
function grid_dc_filter_admin_bar() {
    if( is_admin_bar_showing() ) {
        // Remove comments links from admin bar
        remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
        if( is_multisite() ) {
            add_action( 'admin_bar_menu', array( $this, 'remove_network_comment_links' ), 500 );
        }
    }
}








add_action( 'wp_loaded', 'grid_dc_init_wploaded_filters' );
function grid_dc_init_wploaded_filters(){

    if( is_admin() ) {
        add_action('admin_menu', 'grid_dc_filter_admin_menu', 9999);    // do this as late as possible
        add_action('admin_head', 'grid_dc_hide_dashboard_bits');
        add_action('wp_dashboard_setup', 'grid_dc_filter_dashboard');
        add_filter('pre_option_default_pingback_flag', '__return_zero');
    }else{
        add_filter( 'feed_links_show_comments_feed', '__return_false' );
        add_action( 'wp_footer', 'grid_dc_hide_meta_widget_link', 100 );
    }

}

function grid_dc_filter_admin_menu(){
    global $pagenow;

    if ( $pagenow == 'comment.php' || $pagenow == 'edit-comments.php' || $pagenow == 'options-discussion.php' )
        wp_die( __( 'Comments are closed.' ), '', array( 'response' => 403 ) );

    remove_menu_page( 'edit-comments.php' );
    remove_submenu_page( 'options-general.php', 'options-discussion.php' );
}

function grid_dc_hide_dashboard_bits(){
    if( 'dashboard' == get_current_screen()->id )
        add_action( 'admin_print_footer_scripts', 'grid_dc_filter_dashboard'  );
}

function grid_dc_filter_dashboard(){
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
}

function grid_dc_hide_meta_widget_link(){
    if ( is_active_widget( false, false, 'meta', true ) && wp_script_is( 'jquery', 'enqueued' ) ) {
        echo '<script> jQuery(function($){ $(".widget_meta a[href=\'' . esc_url( get_bloginfo( 'comments_rss2_url' ) ) . '\']").parent().remove(); }); </script>';
    }
}

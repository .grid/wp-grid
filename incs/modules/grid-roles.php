<?php

class grid_roles{

    function __constructor(){
        add_action( 'init', array(&$this, 'create_roles') );
//        add_action( 'admin_menu', array(&$this, 'remove_submenus'), 2);
    }

    function create_roles(){
        global $wp_roles;

        // pour reset le role
        remove_role( 'client' );

        if ( ! isset( $wp_roles ) )
            $wp_roles = new WP_Roles();

        $adm = $wp_roles->get_role('administrator');
        $clientCapabilities = $adm->capabilities;

        // theme
        $clientCapabilities['edit_themes'] = false;
        $clientCapabilities['switch_themes'] = false;
        $clientCapabilities['install_themes'] = false;
        $clientCapabilities['update_themes'] = false;
        $clientCapabilities['delete_themes'] = false;

        // plugins
        $clientCapabilities['activate_plugins'] = false;
        $clientCapabilities['edit_plugins'] = false;
        $clientCapabilities['install_plugins'] = false;
        $clientCapabilities['delete_plugins'] = false;
        $clientCapabilities['update_plugin'] = false;

        // users
//        $clientCapabilities['edit_users'] = false;
//        $clientCapabilities['promote_users'] = false;
//        $clientCapabilities['remove_users'] = false;

        // outil / core
        $clientCapabilities['update_core'] = false;
        $clientCapabilities['export'] = false;
        $clientCapabilities['import'] = false;

        // levels
        $clientCapabilities['level_10'] = false;
        $clientCapabilities['level_9'] = false;
//        $clientCapabilities['level_8'] = false;

        $wp_roles->add_role('client', 'Client', $clientCapabilities);
    }

}

global $grid_roles;
$grid_roles = new grid_roles();

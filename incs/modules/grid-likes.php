<?php
class grid_likes {

    var $needLogin = false; // s'il faut etre connecté pour enregistré les likes (true | false)
    var $maxLikes = 5; // 0 si pas de limite

    var $version = '0.1.0';

    var $cookieName = 'grid-likes';
    var $metaName = '_nblikes';

    function __construct() {

        global $wpdb;

        $this->tablename = $wpdb->prefix.'grid_likes';

        add_action('wp_ajax_nopriv_grid_likes', array(&$this, 'like_post'));
        add_action('wp_ajax_grid_likes', array(&$this, 'like_post'));

        add_action('admin_init', array(&$this, 'check_updates'));

    }

    function check_updates(){
        if(isset($this->version)){
            $prevVersion = get_option( "grid_likes_db_version" );
            if ( $this->version != $prevVersion ) {
                $this->init_tables();
            }
        }
    }

    function init_tables() {
        global $wpdb;

        $sql = 'CREATE TABLE `'.$this->tablename.'` (
          `post_id` int(11) DEFAULT NULL,
          `user_id` int(11) DEFAULT NULL,
          `date` datetime DEFAULT NULL
        )';
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);

        update_option( "grid_likes_db_version", $this->version );
    }

    function get_user_likes($userid){
        global $wpdb;
        $likes = $wpdb->get_results('
            SELECT *
            FROM '.$this->tablename.'
            WHERE user_id="'.$userid.'"
        ');
        return $likes;
    }

    function get_count_like_user($userid){
        global $wpdb;
        $nblikes = $wpdb->get_var('
            SELECT COUNT(*)
            FROM '.$this->tablename.'
            WHERE user_id="'.$userid.'"
        ');
        return $nblikes;
    }

    function get_count_like_post($postid){
        global $wpdb;
        $nblikes = $wpdb->get_var('
            SELECT COUNT(*)
            FROM '.$this->tablename.'
            WHERE post_id="'.$postid.'"
        ');
        return $nblikes;
    }

    function get_post_user_likes($postid, $userid){
        global $wpdb;
        $likeRow = $wpdb->get_row('
            SELECT *
            FROM '.$this->tablename.'
            WHERE user_id="'.$userid.'"
            AND post_id="'.$postid.'"
        ');
        return $likeRow;
    }


    function like_post(){

        global $wpdb;

        $return = array(
            'count'=>0
        );

        if($this->needLogin==false || ($this->needLogin==true && is_user_logged_in())){

            $userId = ($this->needLogin==true) ? get_current_user_id() : 0;

            if(isset($_POST['postid']) && (int)$_POST['postid']>0){
                $postid = $_POST['postid'];


                $cookiePosts =  (isset($_COOKIE[$this->cookieName])) ? json_decode(stripslashes($_COOKIE[$this->cookieName]), true) : array();

                if($userId>0){
                    $returnCookiePost = array();
                    $cookieUserPosts = $this->get_user_likes($userId);
                    if($cookieUserPosts){
                        foreach($cookieUserPosts as $cookieUserPost){
//                            $returnCookiePost[$cookieUserPost->post_id] = (isset($cookiePosts[$cookieUserPost->post_id])) ? $cookiePosts[$cookieUserPost->post_id] : $this->get_count_like_post($cookieUserPost->post_id);
                            $returnCookiePost[$cookieUserPost->post_id] = $this->get_count_like_post($cookieUserPost->post_id);
                        }
                        $cookiePosts = $returnCookiePost;
                    }else{
                        $cookiePosts = array();
                    }
                    $return['count'] = $this->get_count_like_post($postid);
                }else{
                    $return['count'] = (int)get_post_meta($postid, $this->metaName, true);
                }

                $action = 0;
                if(!array_key_exists($postid, $cookiePosts)){
                    if($this->maxLikes>0 && count($cookiePosts)>=$this->maxLikes) {
                        $return['error'] = __('Vous avez atteint le maximum de like', 'grid_lang');
                    }else{
                        $action = 1;
                        $return['count']++;
                        $cookiePosts[$postid] = $return['count'];
                    }
                }else{
                    $action = -1;
                    $return['count']--;
                    unset($cookiePosts[$postid]);
                }

                update_post_meta($postid, $this->metaName, $return['count']);

                // update user meta
                if($userId>0){
                    if($action==1){
                        $wpdb->insert($this->tablename, array(
                            'user_id'=>$userId,
                            'post_id'=>$postid,
                            'date'=>date_i18n('Y-m-d H:i:s')
                        ));
                    }elseif($action==-1){
                        $wpdb->delete($this->tablename, array(
                            'user_id'=>$userId,
                            'post_id'=>$postid
                        ));
                    }
                }

                // update cookie
                setcookie($this->cookieName, json_encode($cookiePosts), null,'/');
//                setcookie($this->cookieName, json_encode($cookiePosts), time()+(60*60*24),'/');

            }else{
                $return['error'] = __('Post introuvable', 'grid_lang');
            }
        }else{
            $return['error'] = __('Vous devez être connecté pour liker', 'grid_lang');
        }

        echo json_encode($return);
        wp_die();
    }


}
global $grid_likes;
$grid_likes = new grid_likes();

<?php

class grid_register_newsletter{

    var $pluginname = 'grid-registernewsletter';

    var $version = '0.1.0';

    function __construct(){
        global $wpdb;

        if(!defined('ACTIVE_NEWSLETTER') || ACTIVE_NEWSLETTER==false)
            return;

        add_action( 'wp_ajax_save_newsletter', array(&$this, 'save_email') );
        add_action( 'wp_ajax_nopriv_save_newsletter', array(&$this, 'save_email') );

        if($this->isMailchimp())
            return false;

        $this->tablename = $wpdb->prefix . 'grid_register_newsletter';

        $this->adminurl = admin_url('options-general.php?page='.$this->pluginname);

        add_action( 'admin_menu', array(&$this, 'addMenu') );

        add_action( 'plugins_loaded', array($this, 'check_updates') );

        add_action( 'admin_init', array(&$this, 'downloadcsv') );

    }

    function check_updates(){
        $prevVersion = get_option( "grid_register_newsletter_db_version" );
        if ( $this->version != $prevVersion ) {
            $this->init_tables();
        }
    }


    function downloadcsv() {

        if(isset($_GET['page']) && $_GET['page']==$this->pluginname && isset( $_GET['download'] )){
            header( "Content-type: application/csv" );
            header( "Content-Disposition: attachment; filename=mails_newsletter.csv" );
            header( "Pragma: no-cache" );
            header( "Expires: 0" );

            $datas = $this->get_registered_emails();
            foreach($datas as $data){
                echo implode( ';', $data ) . "\r\n";
            }

            die;

        }
    }

    function init_tables() {

        global $wpdb;

        $sql = "CREATE TABLE `$this->tablename` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `email` varchar(200) DEFAULT NULL,
          `date` datetime DEFAULT NULL,
          PRIMARY KEY (`id`)
        );";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);

        update_option( "grid_register_newsletter_db_version", $this->version );

    }

    function isMailchimp(){
        return (defined('MAILCHIMP_APIKEY') && defined('MAILCHIMP_LISTID') && MAILCHIMP_APIKEY!='' && MAILCHIMP_LISTID!='');
    }

    function addMenu(){
        $pages[] = add_submenu_page ('options-general.php', __('Inscription Newsletter', 'grid_order'), __('Inscription Newsletter', 'grid_order'), 'manage_options', $this->pluginname, array (&$this, 'page' ) );
    }

    function get_registered_emails($limit = 0){
        global $wpdb;
        $request = 'SELECT * FROM ' . $this->tablename;
        $request .= ' ORDER BY date DESC';
        if($limit>0)
            $request .= ' LIMIT 0,'.$limit;
        $datas = $wpdb->get_results( $request, ARRAY_A );
        return $datas;
    }

    function page(){
        global $wpdb;
        $datas = $this->get_registered_emails(50);
        ?>

        <div class="wrap">
            <h2>Inscription aux Newsletters</h2>

            <p style="padding:15px 0;">
                <a href="<?php echo admin_url('options-general.php?page='.$this->pluginname.'&download');?>" class="button-primary">Télécharger tous les emails (.csv)</a>
            </p>

            <h4>Derniers inscrits</h4>

            <table class="widefat fixed" cellspacing="0">
                <thead>
                    <tr>
                        <th class="manage-column column-columnname" scope="col">ID</th>
                        <th class="manage-column column-columnname" scope="col">Emails</th>
                        <th class="manage-column column-columnname num" scope="col">Date</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th class="manage-column column-columnname" scope="col">ID</th>
                        <th class="manage-column column-columnname" scope="col">Emails</th>
                        <th class="manage-column column-columnname num" scope="col">Date</th>
                    </tr>
                </tfoot>

                <tbody>
                    <?php
                    $alternate = false;
                    foreach($datas as $data):
                        $alternate = ($alternate == false) ? true : false;
                        ?>
                        <tr class="<?php if($alternate) echo 'alternate';?>">
                            <th class="column-columnname" scope="row"><?php echo $data['id'];?></th>
                            <td class="column-columnname"><?php echo $data['email'];?></td>
                            <td class="column-columnname"><?php echo mysql2date('\l\e d/m/Y à H:i:s', $data['date']);?></td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>


            <p style="padding:15px 0;">
                <a href="<?php echo admin_url('options-general.php?page='.$this->pluginname.'&download');?>" class="button-primary">Télécharger tous les emails (.csv)</a>
            </p>

        </div>


        <?php
    }




    function save_email(){

        global $wpdb;

        $result = array();

        if(isset($_POST['email'])){
            $email = trim(strip_tags($_POST['email']));


            if (filter_var($email, FILTER_VALIDATE_EMAIL)){
                if($this->isMailchimp()){

                    require_once get_stylesheet_directory() . '/incs/classes/mailchimp/Mailchimp.php';

                    $MailChimp = new Mailchimp(MAILCHIMP_APIKEY);
                    try {

                        $result = $MailChimp->lists->subscribe(MAILCHIMP_LISTID, array('email' => $email), array(), 'html', false, false, false);

                    } catch (Exception $e) {
                        if (get_class($e) === 'Mailchimp_List_AlreadySubscribed') {
                            $result = array('error' => 'email_already_subscribed');
                        } else {
                            // unknown error, debug purpose only
                            $result = array(
                                'error' => true,
                                'error1' => $e->getMessage(),
                                'error2' => $e->getCode(),
                                'error3' => $e->getFile(),
                                'error4' => get_class($e),
                            );
                        }
                    }

                }else{
                    $rowEmail = $wpdb->get_row('SElECT * FROM '.$this->tablename.' WHERE email="'.$email.'"');
                    if($rowEmail){
                        $result = array('error' => 'email_already_subscribed');
                    }else{
                        $wpdb->insert($this->tablename, array('id'=>'', 'email'=>$email, 'date'=>date_i18n('Y-m-d H:i:s')));
                        $result = array('success' => 'email_registered');
                    }
                }
            }else{
                $result = array('error' => 'email_not_valid');
            }
        }else{
            $result = array('error' => 'no_email');
        }

        wp_send_json( $result );
        die;


    }

}

global $grid_register_newsletter;
$grid_register_newsletter = new grid_register_newsletter();

<?php
class grid_newsletter extends grid_forms {

    var $label = 'Newsletter';
    var $name = 'newsletter';
    var $formDashicon = 'dashicons-email-alt';
    var $sendEmails = array('maxime+meilleurhabitat@grid-agency.com');
    var $version = '0.1.0';
    var $succesMessage = "Vous voilà enregistré à la newsletter";
    var $accuseReception = '';

    function __construct() {
        parent::__construct();
    }

    function getFields(){
        $fields = array(


            'email'=>array(
                'type'=>'email',
                'screenreader' => true,
                'backoffice'=>true,
                'label'=>"J'inscris mon email",
                'sqlinfo'=>'text',
                'check'=>array('required', 'email', 'nodouble'),
                //'icon' => 'icon-envelope'
            ),


        );

        return $fields;
    }


}

global $grid_newsletter;
$grid_newsletter = new grid_newsletter();

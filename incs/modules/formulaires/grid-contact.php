<?php
class grid_contact extends grid_forms {

    var $label = 'Contact';
    var $name = 'contact';
    var $formDashicon = 'dashicons-phone';
    var $sendEmails = array('maxime+meilleurhabitat@grid-agency.com');
    var $version = '0.1.0';
    var $accuseReception = 'email';
    //var $akismetinfos = array('nom', 'email', 'message');

    function __construct() {
        parent::__construct();
    }

    function getFields(){
        $fields = array(
          'civilite'=>array(
              'newstep' => true,
              'screenreader' => true,
              'type'=>'radio',
              'backoffice'=>true,
              'label'=>"Civilité",
              'sqlinfo'=>'text',
              'check'=>array('required'),
              'datas' => array(
                'madame' => 'Madame',
                'monsieur' => 'Monsieur',
              )
          ),
          'nom'=>array(
              'type'=>'text',
              'screenreader' => true,
              'backoffice'=>true,
              'label'=>__('Votre nom', 'grid_lang'),
              'sqlinfo'=>'text',
              'check'=>array('required'),
              'icon' => 'icon-user',
              'classes' => 'medium'
          ),
          'prenom'=>array(
              'type'=>'text',
              'screenreader' => true,
              'backoffice'=>true,
              'label'=>__('Votre prénom', 'grid_lang'),
              'sqlinfo'=>'text',
              'check'=>array('required'),
              'icon' => 'icon-user',
              'classes' => 'medium'
          ),
          'email'=>array(
              'type'=>'email',
              'screenreader' => true,
              'backoffice'=>true,
              'label'=>__('Votre adresse e-mail', 'grid_lang'),
              'sqlinfo'=>'text',
              'check'=>array('required', 'email'),
              'icon' => 'icon-envelope',
              'classes' => 'large'
          ),
          'subject'=>array(
              'type'=>'select',
              'screenreader' => true,
              'backoffice'=>true,
              'label'=>__('Votre demande concerne', 'grid_lang'),
              'sqlinfo'=>'text',
              'check'=>array('required'),
              'datas' => array(
                'value1' => 'Value 1',
                'value2' => 'Value 2',
              ),
              'check'=>array('required'),
              'icon' => 'icon-question'
          ),
          'message'=>array(
              'type'=>'textarea',
              'screenreader' => true,
              'backoffice'=>true,
              'label'=>__('Votre message', 'grid_lang'),
              'sqlinfo'=>'text',
              'check'=>array('required'),
              'icon' => 'icon-pencil'
          ),
        );

        return $fields;
    }


}

global $grid_contact;
$grid_contact = new grid_contact();
